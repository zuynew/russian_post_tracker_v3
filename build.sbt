name := "russian_post_tracker_v3"

version := "1.0"

scalaVersion := "2.11.8"



libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-api" % "1.7.12",
  "org.slf4j" % "slf4j-simple" % "1.7.12"
)


libraryDependencies += "com.typesafe.akka" % "akka-stream-experimental_2.11" % "2.0.3"

libraryDependencies += "net.databinder.dispatch" %% "dispatch-core" % "0.11.3"

libraryDependencies += "org.reactivemongo" %% "reactivemongo" % "0.11.11"
    