import RussianPostXMLBindings.{OperationHistoryRecord, OperationHistoryRecordBSONProtocol}
import reactivemongo.bson.Macros

case class TrackData(barcode: String, operationHistoryData: Seq[OperationHistoryRecord])


object TrackDataBSONProtocol {

  import OperationHistoryRecordBSONProtocol._

  implicit val trackDataBSONProtocolHandler = Macros.handler[TrackData]

}
