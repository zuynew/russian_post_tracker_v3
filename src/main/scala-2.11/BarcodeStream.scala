object BarcodeStream {

  def apply(zipcode: String): Stream[String] = {
    require(zipcode.length == 6)

    apply(zipcode, 1)
  }

  def apply(zipcode: String, month: Int): Stream[String] = {
    require(zipcode.length == 6 && month > 0)

    apply(zipcode, month, 0)

  }

  def apply(zipcode: String, month: Int, number: Int): Stream[String] = {
    require(zipcode.length == 6 && month > 0)

    apply(zipcode, month, number, None)

  }

  def apply(zipcode: String, month: Int, number: Int, lastMonth: Int): Stream[String] = {
    require(zipcode.length == 6 && month > 0 && month < 100 && number >= 0 && number < 100000 && lastMonth > 0 && lastMonth < 100)

    apply(zipcode, month, number, Some((lastMonth, None)))
  }

  def apply(zipcode: String, month: Int, number: Int, lastMonth: Int, lastNumber: Int): Stream[String] = {
    require(zipcode.length == 6 && month > 0 && month < 100 && number >= 0 && number < 100000 && lastMonth > 0 && lastMonth < 100 && lastNumber >= 0 && lastNumber < 100000)

    apply(zipcode, month, number, Some((lastMonth, Some(lastNumber))))
  }

  private def apply(zipcode: String, month: Int, number: Int, lastBarcodeParams: Option[(Int, Option[Int])] = None): Stream[String] = {
    require(zipcode.length == 6 && month > 0 && month < 100 && number >= 0 && number < 100000)

    lastBarcodeParams match {
      case Some((lastMonth, lastNumberOption)) =>
        require( lastMonth > 0 && lastMonth < 100 )
        lastNumberOption match {
          case Some(lastNumber) =>
            require( lastNumber >= 0 && lastNumber < 100000)
          case _ =>
        }
      case _ =>
    }

    lastBarcodeParams match {
      case Some((lastMonth, Some(lastNumber))) if lastMonth == month && lastNumber == number =>
        calcFullBarcode(zipcode + "%02d".format(month) + "%05d".format(number)) #:: Stream.empty
      case Some((lastMonth, None)) if lastMonth == month =>
        Stream.empty

      case _ =>
        val (nextMonth, nextNumber) =
          if ((number + 1) % 100000 == 0)
            (
              if (month % 99 == 0) 1 else month + 1,
              0
              )
          else (month, number + 1)

        calcFullBarcode(zipcode + "%02d".format(month) + "%05d".format(number)) #:: apply(zipcode, nextMonth, nextNumber, lastBarcodeParams)
    }
  }

  private def calcFullBarcode(barcode: String): String = {
    require(barcode.length == 13)

    barcode + calcChecksum(barcode).toString
  }

  private def calcChecksum(barcode: String): Int = {
    require(barcode.length == 13)

    def calcChecksum(barcode: List[Char], oddSum: Int, evenSum: Int, isOdd: Boolean): Int = {
      barcode match {
        case Nil => (10 - ((oddSum * 3 + evenSum) % 10))%10
        case x :: tail =>
          calcChecksum(tail, if (isOdd) oddSum + x.asDigit else oddSum, if (!isOdd) evenSum + x.asDigit else evenSum, !isOdd)
      }
    }
    calcChecksum(barcode.toList, 0, 0, isOdd = true)
  }


}
