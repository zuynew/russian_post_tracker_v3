package RussianPostXMLBindings

import java.util.GregorianCalendar
import javax.xml.datatype.DatatypeFactory

import reactivemongo.bson._

object XMLGregorianCalendarBSONProtocol {

  implicit object XMLGregorianCalendarWriter extends BSONWriter[javax.xml.datatype.XMLGregorianCalendar, BSONDateTime] {
    def write(date: javax.xml.datatype.XMLGregorianCalendar): BSONDateTime = BSONDateTime(date.toGregorianCalendar.getTimeInMillis)
  }

  implicit object XMLGregorianCalendarReader extends BSONReader[BSONDateTime, javax.xml.datatype.XMLGregorianCalendar] {
    def read(date: BSONDateTime): javax.xml.datatype.XMLGregorianCalendar = {
      val df = DatatypeFactory.newInstance()
      val calendar = new GregorianCalendar()
      calendar.setTimeInMillis(date.value)
      df.newXMLGregorianCalendar(calendar)
    }
  }
}

object BigIntBSONProtocol {

  implicit object BigIntWriter extends BSONWriter[BigInt, BSONInteger] {
    def write(number: BigInt): BSONInteger = BSONInteger(number.toInt)
  }

  implicit object BigIntReader extends BSONReader[BSONInteger, BigInt] {
    def read(number: BSONInteger): BigInt = {
      number.asInstanceOf[BigInt]
    }
  }
}

object AddressBSONProtocol {

  implicit val addressBSONProtocolHandler = Macros.handler[Address]

}

object CountryBSONProtocol {

  implicit val countryBSONProtocolHandler = Macros.handler[Country]

}

object AddressParametersBSONProtocol {

  import AddressBSONProtocol._
  import CountryBSONProtocol._

  implicit val addressParametersBSONProtocolHandler = Macros.handler[AddressParameters]

}


object FinanceParametersBSONProtocol {

  import BigIntBSONProtocol._

  implicit val financeParametersBSONProtocolHandler = Macros.handler[FinanceParameters]

}

object Rtm02ParameterBSONProtocol {

  implicit val Rtm02ParameterBSONProtocolHandler = Macros.handler[Rtm02Parameter]

}

object ItemParametersBSONProtocol {

  import Rtm02ParameterBSONProtocol._
  import BigIntBSONProtocol._

  implicit val itemParametersBSONProtocolHandler = Macros.handler[ItemParameters]

}

object OperationParametersBSONProtocol {

  import Rtm02ParameterBSONProtocol._

  import XMLGregorianCalendarBSONProtocol._

  implicit val operationParametersBSONProtocolHandler = Macros.handler[OperationParameters]

}

object UserParametersBSONProtocol {

  import Rtm02ParameterBSONProtocol._

  implicit val userParametersBSONProtocolHandler = Macros.handler[UserParameters]

}

object OperationHistoryRequestBSONProtocol {

  implicit val operationHistoryRequestBSONProtocolHandler = Macros.handler[OperationHistoryRequest]

}

object OperationHistoryRecordBSONProtocol {

  import AddressParametersBSONProtocol._
  import FinanceParametersBSONProtocol._
  import ItemParametersBSONProtocol._
  import OperationParametersBSONProtocol._
  import UserParametersBSONProtocol._

  implicit val operationHistoryRecordBSONProtocolHandler = Macros.handler[OperationHistoryRecord]

}

object OperationHistoryDataBSONProtocol {

  import OperationHistoryRecordBSONProtocol._

  implicit object OperationHistoryDataWriter extends BSONDocumentWriter[OperationHistoryData] {
    def write(operationHistoryData: OperationHistoryData): BSONDocument = {
      BSONDocument("OperationHistoryData" -> operationHistoryData.historyRecord )
    }
  }

  implicit object OperationHistoryDataReader extends BSONDocumentReader[OperationHistoryData] {
    def read(operationHistoryData: BSONDocument): OperationHistoryData = {
      OperationHistoryData(operationHistoryData.get("OperationHistoryData").iterator.map( x => Some(x.asInstanceOf[OperationHistoryRecord])).toSeq : _*)
    }
  }

}

object LanguageBSONProtocol {

  implicit val languageBSONProtocolHandler = Macros.handler[Language]
}

object LanguageDataBSONProtocol {

  implicit object LanguageDataWriter extends BSONDocumentWriter[LanguageData] {
    def write(languageData: LanguageData): BSONDocument =  BSONDocument("Language" -> BSONArray(languageData.Language.map(_.asInstanceOf[BSONValue])))

  }

  implicit object LanguageDataReader extends BSONDocumentReader[LanguageData] {
    def read(languageData: BSONDocument): LanguageData = {
      LanguageData(languageData.get("Language").iterator.map(_.asInstanceOf[Language]).toSeq : _*)
    }
  }

}
