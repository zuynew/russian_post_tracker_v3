// Generated by <a href="http://scalaxb.org/">scalaxb</a>.
package RussianPostXMLBindings


case class GetSmsHistoryResponse(SmsHistoryData: Option[RussianPostXMLBindings.SmsHistoryData] = None)


case class GetLanguagesResponse(LanguageData: Option[RussianPostXMLBindings.LanguageData] = None)


case class GetLanguages(AuthorizationHeader: Option[RussianPostXMLBindings.AuthorizationHeader] = None)


case class GetCustomDutyEventsForMail(AuthorizationHeader: Option[RussianPostXMLBindings.AuthorizationHeader] = None,
  CustomDutyEventsForMailInput: Option[RussianPostXMLBindings.CustomDutyEventsForMailInput] = None)


case class GetOperationHistory(OperationHistoryRequest: Option[RussianPostXMLBindings.OperationHistoryRequest] = None,
  AuthorizationHeader: Option[RussianPostXMLBindings.AuthorizationHeader] = None)


case class PostalOrderEventsForMailResponse(PostalOrderEventsForMaiOutput: Option[RussianPostXMLBindings.PostalOrderEventsForMaiOutput] = None)


case class PostalOrderEventsForMail(AuthorizationHeader: Option[RussianPostXMLBindings.AuthorizationHeader] = None,
  PostalOrderEventsForMailInput: Option[RussianPostXMLBindings.PostalOrderEventsForMailInput] = None)


case class GetCustomDutyEventsForMailResponse(CustomDutyEventsForMailOutput: Option[RussianPostXMLBindings.CustomDutyEventsForMailOutput] = None)


case class GetOperationHistoryResponse(OperationHistoryData: Option[RussianPostXMLBindings.OperationHistoryData] = None)


case class GetSmsHistory(AuthorizationHeader: Option[RussianPostXMLBindings.AuthorizationHeader] = None,
  SmsHistoryRequest: Option[RussianPostXMLBindings.SmsHistoryRequest] = None)

