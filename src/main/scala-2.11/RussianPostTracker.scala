import RussianPostXMLBindings.OperationHistoryRecord
import akka.actor.ActorSystem
import akka.event.Logging
import akka.stream.ThrottleMode.Shaping
import akka.stream._
import akka.stream.scaladsl._
import com.ning.http.client._
import com.typesafe.config.ConfigFactory
import dispatch._
import reactivemongo.api.MongoDriver
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocument
import collection.JavaConverters._

import scala.xml.Node
import scala.concurrent.duration._

object RussianPostTracker
  extends App
{

  val config = ConfigFactory.load()

  implicit val actorSystem = ActorSystem("app")
  implicit val actorMaterializer = ActorMaterializer()


  val DEFAULT_PARALLELISM = config.getInt("parallelism")

  def russianPostRequestFlow(login: String, password: String) = Flow[String].map( barcode => {
    url("https://tracking.russianpost.ru/rtm34")
      .setMethod("POST")
      .setContentType("application/soap+xml", "UTF-8")
      .setBody(
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope" xmlns:ns1="http://russianpost.org/operationhistory/data" xmlns:ns2="http://russianpost.org/operationhistory">
          <SOAP-ENV:Body>
            <ns2:getOperationHistory>
              <ns1:OperationHistoryRequest>
                <ns1:Barcode>{barcode}</ns1:Barcode>
                <ns1:MessageType>0</ns1:MessageType>
                <ns1:Language>RUS</ns1:Language>
              </ns1:OperationHistoryRequest>
              <ns1:AuthorizationHeader>
                <ns1:login>{login}</ns1:login>
                <ns1:password>{password}</ns1:password>
              </ns1:AuthorizationHeader>
            </ns2:getOperationHistory>
          </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>.toString()
      )
  })

  def dispatchFlow(http: Http) = Flow[Req].mapAsync(DEFAULT_PARALLELISM)( req => {

    import dispatch.Defaults._

    retry.Backoff(max = 4, delay = 5 seconds, base = 2) {
      () => {
        http(req).either.map {
          case Left(error) =>
            Left(error)
          case Right(response) =>
            if (response.getStatusCode != 200)
            {
              val error = new Exception(s"Service return ${response.getStatusCode}")
              Left(error)
            }
            else
            {
              Right(response)
            }
        }
      }
    }
  })

  val dispatchResponseFilter = Flow[Either[Throwable, Response]].filter(_.isRight).map(_.right.get)

  val xmlFlow = Flow[Response].map(res => scala.xml.XML.loadString(res.getResponseBody("UTF-8")))

  val operationHistoryRecordFlow = Flow[Node].map(xml => {
    (xml \\ "historyRecord").map(node => scalaxb.fromXML[OperationHistoryRecord](node))
  })

  val trackDataFlow = Flow[Seq[OperationHistoryRecord]].map(operationHistoryRecordSeq => {
    operationHistoryRecordSeq.find(
      operationHistoryRecord => operationHistoryRecord.ItemParameters.exists(_.Barcode.isDefined)
    )
    .flatMap(_.ItemParameters)
    .flatMap(_.Barcode)
    .flatMap(barcode => Some(TrackData(barcode, operationHistoryRecordSeq)))
  })


  import scala.concurrent.ExecutionContext.Implicits.global

  def saveSink(collection: BSONCollection) = Sink.foreachParallel[TrackData](DEFAULT_PARALLELISM){ trackData =>
    import TrackDataBSONProtocol._

    collection.update(
      BSONDocument("barcode" -> trackData.barcode),
      trackData,
      upsert = true
    )
  }

  val barcodeStream = Flow[String].mapConcat( zipcode => {
    BarcodeStream(zipcode, 80)
  })

  Source(scala.io.Source.fromFile(config.getString("indexes_file_path")).getLines().toStream)
    .withAttributes(
      ActorAttributes
        .supervisionStrategy(Supervision.resumingDecider)
        .and(
          ActorAttributes.logLevels(onFinish = Logging.InfoLevel)
        )
    )
    .log("Extracted zipcode")
    .via(barcodeStream)
    .log("Extracted barcode")
    .via(russianPostRequestFlow(config.getString("post_api.login"), config.getString("post_api.password")))
    .log("Created request")
    .throttle(config.getInt("post_api.rps"), 1 day, 1, Shaping)
    .log("Request throttled")
    .via(
      dispatchFlow(
        Http.configure(
          _.setAllowPoolingConnections(true)
            .setConnectTimeout(config.getInt("post_api.timeout").seconds.toMillis.toInt)
        )
      )
    )
    .log("Got http response")
    .via(dispatchResponseFilter)
    .via(xmlFlow)
    .log("Parsed xml")
    .via(operationHistoryRecordFlow)
    .log("Mapped xml to history records")
    .via(trackDataFlow)
    .mapConcat(_.toList)
    .log("Mapped history records to track data")
    .runWith(saveSink(
      new MongoDriver()
        .connection(config.getStringList("mongo.nodes").asScala.toList)(config.getString("mongo.db"))
        .collection(config.getString("mongo.collection"))
    )).onComplete(_ => actorSystem.shutdown())



  actorSystem.awaitTermination()
}
